from src.task_master.flaskr.main import check_task_is_ok
import pytest


@pytest.mark.unittest
def test_len_more_than_0():
    content = ""
    assert check_task_is_ok(content) == False

@pytest.mark.unittest
def test_len_less_than_201():
    content=str(['a' for i in range(0,300)])
    assert check_task_is_ok(content) == False

@pytest.mark.unittest
def test_len_in_range():
    content = "ciao"
    assert check_task_is_ok(content)


