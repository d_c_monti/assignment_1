import pytest
from pathlib import Path

from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from src.task_master.database.models import init_db
from src.task_master.database import models
from src.task_master.providers.task_provider import TaskProvider
from src.task_master.flaskr import create_app
from src.task_master.flaskr import main
class TestClass:
    @classmethod
    def setup(self):
        db_dir = Path(__file__).parent.parent
        db_file = db_dir / Path('src/task_master/database/task_master.sqlite')

        engine = create_engine(f'sqlite:///{db_file}')

        if not db_file.exists():
            init_db(engine)
        self.task_provider = TaskProvider(engine)
        self.engine = engine
        app = create_app()
        self.app = app.test_client()
    
    @pytest.mark.integtest
    def test_post_request_added_to_db(self):
        self.task_provider.add_task("integration_test_content 1")
        rv = self.app.get('/', follow_redirects=True)
        rv_post = self.app.post('/', data=dict(
            content='integration_test_content 2'
        ))
        tasks = self.task_provider.get_tasks()
        tasks_content = [task.content for task in tasks]
        with Session(self.engine) as session, session.begin():
             session.query(models.Todo).filter(models.Todo.content == "integration_test_content 1").delete()
             session.query(models.Todo).filter(models.Todo.content == "integration_test_content 2").delete()
        assert rv.status_code == 200
        assert "integration_test_content 1" in tasks_content
        assert "integration_test_content 2" in tasks_content