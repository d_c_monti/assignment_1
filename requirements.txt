Flask==2.2.2
Flask-SQLAlchemy==3.0.0
SQLAlchemy~=1.4.41
setuptools~=63.2.0
gunicorn==20.1.0