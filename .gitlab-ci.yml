# Added runner
# This file is a template, and might need editing before it works on your project.
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Python.gitlab-ci.yml

# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: python:latest

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/topics/caching/
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache/pip
    - venv/

before_script:
  - python --version  # For debugging
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate 
  - pip install pytest
  - pip install prospector
  - pip install bandit

stages:
  - build
  - verify
  - test
  - package
  - release
  - deploy

build:
  stage: build
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' || $CI_PIPELINE_SOURCE == 'push'
  script:
    - pip install -r requirements.txt

code-quality:
  stage: verify
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' || $CI_COMMIT_BRANCH =~ /^feature_.*$/
  script:
    - python -m prospector --uses flask --with-tool bandit src

unit-test:
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^feature_.*$/ || $CI_PIPELINE_SOURCE == 'merge_request_event'
  script:
    - pytest -m unittest
  artifacts:
    paths:
      - dist/*.whl

integration-test:
  stage: test
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' || $CI_COMMIT_BRANCH == "develop"
  script:
    - pytest -m integtest
  artifacts:
    paths:
      - dist/*.whl

package:
  stage: package
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  script:
    - pip install build
    - pip install twine
    - python -m build --sdist
    - python -m build --wheel
    - twine check dist/*
  artifacts:
    paths:
      - dist/*

release:
  stage: release
  needs: [package]
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  script: 
    - twine upload -u $PYPI_USERNAME -p $PYPI_PASSWORD --skip-existing dist/*
  artifacts:
    paths:
      - dist/*

deploy:
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  before_script:
    - apt-get update -y
    - apt-get install -y curl git gnupg
    - curl https://cli-assets.heroku.com/install-ubuntu.sh | sh
    - |
      cat >~/.netrc <<EOF
      machine api.heroku.com
        login $HEROKU_EMAIL
        password $HEROKU_PRODUCTION_KEY
      machine git.heroku.com
        login $HEROKU_EMAIL
        password $HEROKU_PRODUCTION_KEY
      EOF
    - chmod 600 ~/.netrc
    - heroku git:remote --app $HEROKU_APP_NAME
  script:
    - git checkout $CI_COMMIT_SHA
    - git push heroku $CI_COMMIT_SHA:main
