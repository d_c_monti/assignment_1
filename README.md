# README assignment1 gruppo CMST

## URL REPOSITORY

<https://gitlab.com/d_c_monti/2022_assignment1_CMST>

## COMPONENTI GRUPPO

* Davide Monti
* Samuele Campanella
* Lorenzo Titta
* Davide Soldati

## DESCRIZIONE APP

L'applicazione permette ad un utente di gestire un elenco di attività.
Collegandosi all'applicazione l'utente può visualizzare le attività ancora da svolgere, cancellarla se svolta oppure modificarla.

### Tecnologie utilizzate

TaskMaster è stata realizzata con Python utilizzando il framework Flask per quanto riguarda la gestione del front-end e della logica applicativa.\
Il database invece è stato realizzato con SQLAlchemy, una libreria Python che permette di astrarre il codice sql necessario per la realizzazione del database

## DESCRIZIONE JOBS

### before_script

Questa fase si occupa di iniziallizzare e avviare l'ambiente virtuale necessario al funzionamento dell'applicazione.

### build

Essendo Python un linguaggio interpretato, non è presente una fase di compilazione in questo job, che si limita semplicemente a preparare l'ambiente virtuale predisposto nella fase ```before_script```. Ciò si riduce all'installare le librerie necessarie al funzionamento della nostra app, elencate nel file ```requirements.txt``` attraverso il comando:

```bash
script:
    - pip install -r requirements.txt
```

### code_quality

In questo job vengono eseguiti i principali controlli di linting, come da consegna si è scelto di utilizzare prospector e bandit con il comando:

```bash
script:
    - python -m prospector --uses flask --with-tool bandit src
```

### test

La fase di testing è stata divisa in due parti, unit test e integration test.
Per fare ciò i vari casi di test sono stati segnati con i rispettivi markers ```@pytest.mark.integtest``` e ```@pytest.mark.unittest``` in modo tale da poterli eseguire indipendente, attraverso ```pytest```, con i comandi:

```bash
 - pytest -m unittest
```

```bash
 - pytest -m integtest
```

### package

In questa fase viene creato il package che verrà poi pubblicato su Pypi. Per fare questo abbiamo utilizzato ```build``` per quanto riguarda la creazione del package vero e proprio e ```twine``` per controllare che il package creato non presenti errori. Nel caso in cui il controllo di ```twine``` venga superato si passa alla fase di release.

### release

Questo job deve attendere il completamento di ```package```, questo è stato fatto inserendo:

```bash
needs: [package]
```

Per gestire la release vera e propria si procede con il comando

```bash
script: 
    - twine upload -u $PYPI_USERNAME -p $PYPI_PASSWORD --skip-existing dist/*
```

Per evitare di inserire username e password in chiaro sono state configurate due variabili all'interno del progetto per mascherarle (```PYPI_USERNAME``` e ```PYPI_PASSWORD```).

### deployment

Per gestire il deployment abbiamo optato per Heroku. Per fare questo quindi è necessario installare la sua Command Line Interface, loggarsi con le proprie credenziali e collegare il repository a Heroku:

```bash
- curl https://cli-assets.heroku.com/install-ubuntu.sh | sh
- |
    cat >~/.netrc <<EOF
    machine api.heroku.com
        login $HEROKU_EMAIL
        password $HEROKU_PRODUCTION_KEY
    machine git.heroku.com
        login $HEROKU_EMAIL
        password $HEROKU_PRODUCTION_KEY
    EOF
- chmod 600 ~/.netrc
- heroku git:remote --app $HEROKU_APP_NAME
```

Anche in questo caso si sono usate delle variabili per mascherare username e password. \
Una volta predisposto il collegamento ad Heroku, si può procedere con il deployment vero e proprio, attraverso i comandi:

```bash
script:
    - git checkout $CI_COMMIT_SHA
    - git push heroku $CI_COMMIT_SHA:main
```

## PIPELINE

Abbiamo pensato di organizzare il repository secondo il modello git flow.\
Di conseguenza il repository avrà diversi branch per lo sviluppo e l'implementazione di singole feature, un branch
develop su cui avverrà la merge delle varie feature una volta concluso il loro sviluppo e infine un branch main che
coinciderà con la versione di release. \
L'idea è quella di avere il lavoro di sviluppo distribuito in diversi rami ```feature```. Il ramo ```develop```, invece,  farà da punto di raccordo tra le diverse feature, ma senza avere codice implementato direttamente su di lui.
In modo analogo, una volta concluso un ciclo di sviluppo, il ramo ```main``` riceverà le nuove features esclusivamente dal ramo di developement e, se ritenute ben fatte, si occuperà della fase di deployment. \
Per questo motivo utilizzeremo pipelines diverse: \
\
<img src="https://www.bitbull.it/blog/git-flow-come-funziona/gitflow-1.png" alt="drawing" style="width:500px;"/>

### Pipeline merge_request

Viene eseguita nel momento in cui si effettua una merge_request. Questa pipeline eseguirà le fasi di:

* Build
* Code-Quality
* Unit-Test
* Integration-Test

Questo servirà per un quality check del codice prodotto nel ramo di origine, se la pipeline viene eseguita si può procedere con la merge.
Per far si che questi jobs vengano eseguiti ad ogni merge request, abbiamo inserito per ciascuno di essi la regola:

```bash
if: $CI_PIPELINE_SOURCE == 'merge_request_event'
```

### Pipeline feature

Viene eseguita in tutti i rami feature. Di conseguenza abbiamo deciso che la pipeline si occuperà di eseguire le fasi di:

* Build
* Code-Quality
* Unit-Test

In questo tipo di branch verranno implementate tutte le varie feature, una per ciascuno di essi. Di conseguenza, essendo questi i branch in cui verrà svolta la maggior parte dello sviluppo di nuovo software, abbiamo pensato di costruire la pipeline in modo che faccia da garante della qualità del codice prodotto.
Per nostra convenzione, i rami feature vengono identificati da un nome del tipo  ```feature_<feature_name>```.
Per poterli riconoscere quindi abbiamo utilizzato un'espressione regolare.
Infatti nei jobs interessati è stata inserita la regola:

```bash
 if: $CI_COMMIT_BRANCH =~ /^feature_.*$/
```

### Pipeline develop

Sebbene, per come sia stato pensato il repository, non ci dovrebbero essere commit direttamente sul ramo develop diverse da merge request provenienti dai rami feature, è comunque presente una semplice pipeline che esegue gli integration test, come forma di precauzione.

### Pipeline master

Si occupa della fase di release e deployment e viene esguita alla fine di un ciclo di sviluppo.
Per questo motivo in questa pipeline vengono eseguite le fasi di:

* Build
* Package
* Release
* Deployment

Questa pipeline viene eseguita in seguito ad una merge request dal ramo develop al ramo main, durante la merge request verrà eseguita la pipeline ```merge_request``` e, nel caso in cui tutti i jobs risulteranno superati, si potrà procedere in modo automatico al rollout delle nuove features.

## ESEMPIO DI UTILIZZO

Per rendere più chiaro come abbiamo pensato di impostare le pipelines, possiamo prendere in considerazione  l'implementazione di due features di esempio (```feature_1``` e ```feature_2```). \
L'idea è di avere la fase di implementazione di ciascuna feature eseguita in parallelo in due branch di tipo ```feature```. Durante questa fase, ogni push in questi branch determinerà l'esecuzione della ```pipeline feature``` per verificare la qualità del codice appena realizzato.\
Una volta conclusa la fase di implementazione della feature, lo sviluppatore procederà con una merge request sul ramo ```develop``` che lancerà la ```pipeline merge_request``` e, se i tutti i jobs verranno superati, si procederà con l'effettiva merge in modo automatico. \
Una volta compiuta questa operazione per ciascuna feature dell'iterazione corrente, si potrà procedere con una merge request sul ramo main, anche in questo caso verrà eseguita la ```pipeline merge_request``` e, se verrà superata, verrà eseguita anche la ```pipeline master``` che si occuperà della fase di release e deployment.

<img src=img/git_branches.png alt="Esempio di gestione branch e pipelines" style="width:500px;"/>