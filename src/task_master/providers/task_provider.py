from sqlalchemy.orm import Session
# import src.task_master.database.models as models
from src.task_master.database import models


class TaskProvider:
    def __init__(self, engine):
        self.engine = engine

    def add_task(self, task_content):
        task = models.Todo(
            content=task_content
        )
        with Session(self.engine) as session, session.begin():
            session.add(task)

    def get_task(self, task_id):
        with Session(self.engine) as session, session.begin():
            session.expire_on_commit = False
            task = session.query(models.Todo).filter(models.Todo.id == task_id).first()
        return task

    def get_tasks(self):
        with Session(self.engine) as session, session.begin():
            session.expire_on_commit = False
            tasks = session.query(models.Todo).order_by(models.Todo.date_created).all()
            tasks_list = list(tasks)
        return tasks_list

    def delete_task(self, task_id):
        with Session(self.engine) as session, session.begin():
            session.query(models.Todo).filter(models.Todo.id == task_id).delete()

    def update_task(self, task_id, task_content):
        with Session(self.engine) as session, session.begin():
            query = session.query(models.Todo).filter(models.Todo.id == task_id)
            query.update(
                {
                    models.Todo.content: task_content
                }
            )
