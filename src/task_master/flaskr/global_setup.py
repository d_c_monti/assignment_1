from src.task_master.database import engine
from src.task_master.providers.task_provider import TaskProvider

task_provider = TaskProvider(engine)