from flask import Blueprint, render_template, redirect, url_for, request, flash
import src.task_master.flaskr.global_setup as setup

bp = Blueprint('main', __name__)


def check_task_is_ok(task_content):
    if len(task_content) > 201 or len(task_content) < 1:
        return False
    return True


@bp.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        return index_post()

    return index_get()


def index_post():
    task_content = request.form['content'].strip()
    task_is_ok = check_task_is_ok(task_content)
    if task_is_ok:
        setup.task_provider.add_task(task_content)
        flash('Task succesfully added')
        return redirect(url_for('main.index'))

    flash('Cannot add your task', 'error')
    return redirect(url_for('main.index'))


def index_get():
    tasks = setup.task_provider.get_tasks()
    return render_template('index.html', tasks=tasks)


@bp.route('/delete/<int:task_id>')
def delete(task_id):
    try:
        setup.task_provider.delete_task(task_id)
        return redirect('/')
    except Exception:
        return 'There was a problem deleting your task'


@bp.route('/update/<int:task_id>', methods=['GET', 'POST'])
def update(task_id):
    if request.method == 'POST':
        task_content = request.form['content']
        task_is_ok = check_task_is_ok(task_content)
        if task_is_ok:
            try:
                setup.task_provider.update_task(task_id, task_content)
                flash('Task update')
                return redirect('/')
            except Exception:
                return 'There was an issue updating your task'
        else:
            flash('Content not valid', 'error')
            return redirect('/')
    else:
        task = setup.task_provider.get_task(task_id)
        return render_template('update.html', task = task)
