import pathlib
import sys

from flask import Flask


current = pathlib.Path(__file__).resolve()
project_root = current.parent.parent.parent.parent
sys.path.append(str(project_root))


def config_app():
    return 'fd27835ea6ab3d57137c0e3e'


def create_app():
    # create and configure the app
    app = Flask(__name__)
    app.config['SECRET_KEY'] = config_app()

    from . import main

    app.register_blueprint(main.bp)
    return app
