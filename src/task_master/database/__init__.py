from pathlib import Path

from sqlalchemy import create_engine

from src.task_master.database.models import init_db

db_dir = Path(__file__).parent
db_file = db_dir / Path('task_master.sqlite')

engine = create_engine(f'sqlite:///{db_file}')

if not db_file.exists():
    init_db(engine)
