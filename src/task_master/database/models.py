from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime

Base = declarative_base()

class Todo(Base):
    __tablename__ = 'todo'
    id = Column(Integer, primary_key=True)
    content = Column(String(200), nullable=False)
    date_created = Column(DateTime, default=datetime.utcnow)

def init_db(engine):
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)